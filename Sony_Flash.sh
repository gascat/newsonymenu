#!/bin/bash

ans=$(zenity --list --text "Sony Flasher" --radiolist --column "pick" --column "Year" TRUE "2019" FALSE "2018" FALSE "2017" FALSE "2016" FALSE "2015" FALSE "2014" FALSE "2013" FALSE "Tablets" );
echo $ans
if [ $ans == "2019" ]; then
	/bin/bash ~/NewSonyMenu/res/2019.sh 
elif [ $ans == "2018" ]; then
	/bin/bash ~/NewSonyMenu/res/2018.sh
elif [ $ans == "2017" ]; then
	/bin/bash ~/NewSonyMenu/res/2017.sh
elif [ $ans == "2016" ]; then
	/bin/bash ~/NewSonyMenu/res/2016.sh
elif [ $ans == "2015" ]; then
	/bin/bash ~/NewSonyMenu/res/2015.sh
elif [ $ans == "2014" ]; then
	/bin/bash ~/NewSonyMenu/res/2014.sh
elif [ $ans == "2013" ]; then
	/bin/bash ~/NewSonyMenu/res/2013.sh
elif [ $ans == "Tablets" ]; then
	/bin/bash ~/NewSonyMenu/res/tablets.sh

else 
	zenity --error --text="No year chosen"
	exit
fi
