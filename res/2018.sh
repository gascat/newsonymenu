#!/bin/bash

ans=$(zenity --list --text "Sony Flasher" --radiolist --column "pick" --column "Model" TRUE "XZ3" FALSE "XZ3_Dual" FALSE "XZ2_Premium" FALSE "XZ2_Compact" FALSE "XZ2" FALSE "XZ2_Dual" FALSE "XA2_Plus" FALSE "XA2_Ultra" FALSE "XA2" FALSE "XA2_Dual" FALSE "L2" );
echo $ans
if [ $ans == "XZ3" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2018/xz3.sh 
elif [ $ans == "XZ3_Dual" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2018/xz3d.sh
elif [ $ans == "XZ2_Premium" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2018/xz2p.sh
elif [ $ans == "XZ2_Compact" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2018/xz2c.sh
elif [ $ans == "XZ2" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2018/xz2.sh
elif [ $ans == "XZ2_Dual" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2018/xz2d.sh
elif [ $ans == "XA2_Plus" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2018/xa2p.sh
elif [ $ans == "XA2_Ultra" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2018/xa2u.sh
elif [ $ans == "XA2" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2018/xa2.sh
elif [ $ans == "XA2_Dual" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2018/xa2d.sh
elif [ $ans == "L2" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2018/l2.sh
else 
	zenity --error --text="No model chosen"
	exit
fi

