#!/bin/bash

ans=$(zenity --list --text "Sony Flasher" --radiolist --column "pick" --column "Model" TRUE "XZ" FALSE "X_Performance" FALSE "X_Compact" FALSE "X" FALSE "XA_Ultra" FALSE "XA" FALSE "E5" );
echo $ans
if [ $ans == "XZ" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2019/XP10.sh 
elif [ $ans == "X_Performance" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2016/xp.sh
elif [ $ans == "X_Compact" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2016/xc.sh
elif [ $ans == "X" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2016/x.sh
elif [ $ans == "XA_Ultra" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2016/xau.sh
elif [ $ans == "XA" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2016/xa.sh
elif [ $ans == "E5" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2016/e5.sh
else 
	zenity --error --text="No model chosen"
	exit
fi
