#!/bin/bash

ans=$(zenity --list --text "Sony Flasher" --radiolist --column "pick" --column "Model" TRUE "Z1" FALSE "Z_Ultra" FALSE "Z" );
echo $ans
if [ $ans == "Z1" ]; then
	/bin/bash z1.sh 
elif [ $ans == "Z_Ultra" ]; then
	/bin/bash zu.sh
elif [ $ans == "Z" ]; then
	/bin/bash z.sh
else 
	zenity --error --text="No model chosen"
	exit
fi
