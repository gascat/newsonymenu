#!/bin/bash

ans=$(zenity --list --text "Sony Flasher" --radiolist --column "pick" --column "Model" TRUE "Z5_Premuim" FALSE "Z5_Compact" FALSE "Z5" FALSE "Z3+" FALSE "C5_Ultra" FALSE "C4" FALSE "C4_Dual" FALSE "M5" FALSE "M4_Aqua" FALSE "M2" FALSE "E4" FALSE "E4G" );
echo $ans
if [ $ans == "Z5_Premium" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2015/z5p.sh 
elif [ $ans == "Z5_Compact" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2015/z5c.sh
elif [ $ans == "Z5" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2015/z5.sh
elif [ $ans == "Z3+" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2015/z3p.sh
elif [ $ans == "C5_Ultra" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2015/c5u.sh
elif [ $ans == "C4" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2015/t3.sh
elif [ $ans == "C4_Dual" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2015/t2u.sh
elif [ $ans == "M5 *WARNING*" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2015/m5.sh
elif [ $ans == "M4_Aqua" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2015/m4a.sh
elif [ $ans == "M4" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2015/m4.sh
elif [ $ans == "E4" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2015/e4.sh
elif [ $ans == "E4G" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2015/e4g.sh
else 
	zenity --error --text="No model chosen"
	/bin/bash ~/NewSonyMenu/Sony_Flash.sh
fi
