#!/bin/bash

ans=$(zenity --list --text "Sony Flasher" --radiolist --column "pick" --column "Model" TRUE "XP10" FALSE "XP5" FALSE "XP1" FALSE "L3" );
echo $ans
if [ $ans == "XP10" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2019/XP10.sh 
elif [ $ans == "XP5" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2019/XP5.sh
elif [ $ans == "XP1" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2019/XP1.sh
elif [ $ans == "L3" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2019/L3.sh
else 
	zenity --error --text="No model chosen"
	exit
fi

