#!/bin/bash

ans=$(zenity --list --text "Sony Flasher" --radiolist --column "pick" --column "Model" TRUE "XZ1_Compact" FALSE "XZ1" FALSE "XZ_Premium" FALSE "XA1_Plus" FALSE "XA1_Ultra" FALSE "XA1" FALSE "XA1_Dual" FALSE "L1" );
echo $ans
if [ $ans == "XZ1_Compact" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2017/xz1c.sh 
elif [ $ans == "XZ1" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2017/xz1.sh
elif [ $ans == "XZ_Premium" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2017/xzp.sh
elif [ $ans == "XA1_Plus" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2017/xa1p.sh
elif [ $ans == "XA1_Ultra" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2017/xa1u.sh
elif [ $ans == "XA1" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2017/xa1.sh
elif [ $ans == "XA1_Dual" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2017/xa1d.sh
elif [ $ans == "L1" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2017/l1.sh
else 
	zenity --error --text="No model chosen"
	exit
fi

