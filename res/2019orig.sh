#!/bin/bash

ans=$(zenity --list --text "Sony Flasher" --radiolist --column "pick" --column "Model" TRUE "Xperia 10" FALSE "Xperia 5" FALSE "Xperia 1" FALSE "Xperia L3" );
echo $ans
if [ $ans == "Xperia 10" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2019/XP10.sh 
elif [ $ans == "Xperia 5" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2019/XP5.sh
elif [ $ans == "Xperia 1" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2019/XP1.sh
elif [ $ans == "Xperia L3" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2019/L3.sh
else 
	zenity --error --text="No model chosen"
	exit
fi

