#!/bin/bash

ans=$(zenity --list --text "Sony Flasher" --radiolist --column "pick" --column "Model" TRUE "Z3_Compact" FALSE "Z3" FALSE "Z2" FALSE "Z1_Compact" FALSE "C3" FALSE "T3" FALSE "T2_Ultra" FALSE "M2_Aqua" FALSE "M2" FALSE "E3" FALSE "E1" );
echo $ans
if [ $ans == "Z3_Compact" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2014/z3c.sh 
elif [ $ans == "Z3" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2014/z3.sh
elif [ $ans == "Z2" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2014/z2.sh
elif [ $ans == "Z1_Compact" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2014/z1c.sh
elif [ $ans == "C3" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2014/c3.sh
elif [ $ans == "T3" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2014/t3.sh
elif [ $ans == "T2_Ultra" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2014/t2u.sh
elif [ $ans == "M2_Aqua" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2014/m2a.sh
elif [ $ans == "M2" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2014/m2.sh
elif [ $ans == "E3" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2014/e3.sh
elif [ $ans == "E1" ]; then
	/bin/bash ~/NewSonyMenu/res/models/2014/e1.sh
else 
	zenity --error --text="No model chosen"
	exit
fi
